/*
 * Copyright 2014 CISIAD, UNED, Spain
 *
 * Licensed under the European Union Public Licence, version 1.1 (EUPL)
 *
 * Unless required by applicable law, this code is distributed
 * on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
 */

package org.openmarkov.inference.decompositionIntoSymmetricDANs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import org.openmarkov.core.exception.IncompatibleEvidenceException;
import org.openmarkov.core.exception.InvalidStateException;
import org.openmarkov.core.exception.NonProjectablePotentialException;
import org.openmarkov.core.exception.NotEvaluableNetworkException;
import org.openmarkov.core.exception.UnexpectedInferenceException;
import org.openmarkov.core.exception.WrongCriterionException;
import org.openmarkov.core.inference.InferenceAlgorithm;
import org.openmarkov.core.inference.annotation.InferenceAnnotation;
import org.openmarkov.core.model.graph.Link;
import org.openmarkov.core.model.network.EvidenceCase;
import org.openmarkov.core.model.network.Finding;
import org.openmarkov.core.model.network.Node;
import org.openmarkov.core.model.network.NodeType;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.core.model.network.ProbNetOperations;
import org.openmarkov.core.model.network.State;
import org.openmarkov.core.model.network.Variable;
import org.openmarkov.core.model.network.potential.Intervention;
import org.openmarkov.core.model.network.potential.Potential;
import org.openmarkov.core.model.network.potential.PotentialRole;
import org.openmarkov.core.model.network.potential.TablePotential;
import org.openmarkov.core.model.network.potential.operation.DiscretePotentialOperations;
import org.openmarkov.core.model.network.type.DecisionAnalysisNetworkType;

@InferenceAnnotation(name = "DSD")
public class DecompositionAlgorithm extends InferenceAlgorithm{

	private class Scenario
	{
		private List<Variable> variables;
		private List<Integer> states;

		public Scenario(HashMap<Variable, Integer> map)
		{
			this.variables = new ArrayList<>(map.keySet());
			Collections.sort(this.variables);
			this.states = new ArrayList<>(variables.size());
			for(Variable variable : variables)
			{
				states.add(map.get(variable));
			}
		}

		@Override
		public int hashCode() {
			return variables.hashCode() + 17 * states.hashCode();
		}

		@Override
		public boolean equals(Object obj) {
			if(!(obj instanceof Scenario))
				return false;
			Scenario otherScenario = (Scenario) obj;
			return variables.equals(otherScenario.variables) && states.equals(otherScenario.states);
		}
	}
	
	private class PartialResult
	{
		public TablePotential utilityPotential;
		public TablePotential probabilityPotential;
		
		public PartialResult(TablePotential utilityPotential, TablePotential probabilityPotential) {
			super();
			this.utilityPotential = utilityPotential;
			this.probabilityPotential = probabilityPotential;
		}
		
	}
	
	public static void checkEvaluability(ProbNet probNet) throws NotEvaluableNetworkException {
		if(!(probNet.getNetworkType() instanceof DecisionAnalysisNetworkType))
		{
			throw new NotEvaluableNetworkException("Network " + probNet.getName() + " cannot be evaluated with DSD because it is not a DAN.");
		}
	}
	
	private boolean useCache = true;
	private Map<Variable, Potential> strategy = null;
	private boolean isResolved = false;
	private TablePotential globalUtility = null;
	private Map<Scenario, PartialResult> cache;
	private List<Variable> restrictedVariablePool;
	
	public DecompositionAlgorithm(ProbNet probNet) throws NotEvaluableNetworkException {
		super(probNet);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Potential getOptimizedPolicy(Variable decisionVariable)
			throws IncompatibleEvidenceException, UnexpectedInferenceException {
		if(!isResolved)
			resolve();
		return strategy.get(decisionVariable);
	}

	@Override
	public Potential getExpectedUtilities(Variable decisionVariable)
			throws IncompatibleEvidenceException, UnexpectedInferenceException {
		if(!isResolved)
			resolve();
		return null;
	}

	@Override
	public TablePotential getGlobalUtility() throws IncompatibleEvidenceException,
			UnexpectedInferenceException {
		if(!isResolved)
			resolve();
		return globalUtility;
	}

	@Override
	public HashMap<Variable, TablePotential> getProbsAndUtilities()
			throws IncompatibleEvidenceException, UnexpectedInferenceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HashMap<Variable, TablePotential> getProbsAndUtilities(List<Variable> variablesOfInterest)
			throws IncompatibleEvidenceException, UnexpectedInferenceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TablePotential getJointProbability(List<Variable> variables)
			throws IncompatibleEvidenceException, UnexpectedInferenceException {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void useCache(boolean enable)
	{
		this.useCache = enable;
	}
	
	private void resolve() {
		cache = new HashMap<>();
		restrictedVariablePool = new ArrayList<>();
		//probNet = BasicOperations.removeSuperValueNodes(probNet, new EvidenceCase());
		DecompositionGraphNode root = resolve (probNet, null);
		//potentialSizeLogger(cache);
		globalUtility = root.getExpectedUtility();
		try {
			TablePotential jointProb = root.getScenarioProbability();
			List<Variable> jointProbVariables = jointProb.getVariables();
			for(Variable variable : jointProbVariables)
			{
				TablePotential conditionalProb = new TablePotential(jointProb);
				DiscretePotentialOperations.normalize(conditionalProb);
				List<TablePotential> potentials = new ArrayList<>();
				potentials.add(conditionalProb);
				potentials.add(globalUtility);
				globalUtility = DiscretePotentialOperations.sumOutVariable(variable, potentials).get(0);
				jointProb = DiscretePotentialOperations.marginalize(jointProb, variable);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}				
	}
		
    @SuppressWarnings("unused")
	private void potentialSizeLogger(Map<Scenario, PartialResult> cache) {
		System.out.println(cache.size());
		long probabilityPotentialSize = 0;
		Map<Integer, List<Integer>> utilityPotentialSizes = new HashMap<>();
		long utilityPotentialSize = 0;
		for(Scenario scenario : cache.keySet())
		{
			int numVariablesInScenario = scenario.variables.size();
			PartialResult result = cache.get(scenario);
			probabilityPotentialSize += result.probabilityPotential.getTableSize();
			utilityPotentialSize += result.utilityPotential.getTableSize();
			
			int numVariables = result.utilityPotential.getNumVariables();
			if (!utilityPotentialSizes.containsKey(numVariables))
			{
				List<Integer> counter = new ArrayList<Integer>(11);
				for(int i=0; i<11; ++i)
					counter.add(0);
				utilityPotentialSizes.put(numVariables, counter);
			}
			List<Integer> counter = utilityPotentialSizes.get(numVariables);
			counter.set(numVariablesInScenario, counter.get(numVariablesInScenario) +1);
		}
		System.out.println("Utility potential sizes");
		for(Integer numVariables : utilityPotentialSizes.keySet())
		{
			List<Integer> counter = utilityPotentialSizes.get(numVariables);
			System.out.print(numVariables+"=");
			for(int i=0; i<11; ++i)
				System.out.print(counter.get(i)+",");
			System.out.println();
		}
		System.out.println(probabilityPotentialSize);
		System.out.println(utilityPotentialSize);
		System.out.println((utilityPotentialSize+probabilityPotentialSize));
	}

	/**
     * Builds a decision tree from a decision analysis network
     * @param probNet
     * @return
     */    
    private DecompositionGraphNode resolve (ProbNet probNet, DecompositionGraphNode parentNode)
    {
    	DecompositionGraphNode root = null;
    	try
    	{
			Scenario scenario = new Scenario(getScenarioMap(parentNode));
			if(!useCache || !cache.containsKey(scenario))
			{

				List<Node> alwaysObservedNodes = ProbNetOperations.getAlwaysObservedVariables (probNet);
				List<Variable> alwaysObservedVariables = ProbNet.getVariables(alwaysObservedNodes);
				alwaysObservedVariables.removeAll(scenario.variables);

				if(ProbNetOperations.hasStructuralAsymmetry(probNet) || ProbNetOperations.hasOrderAsymmetry(probNet))
		    	{
		    		List<Node> asymmetricObservedNodes = getAsymmetricObservableNodes(alwaysObservedNodes);
		    		if(!asymmetricObservedNodes.isEmpty())
		    		{
		    			// Calculate optimal tree for all values of the observed node, join them multiplying probability?
		    			List<DecompositionGraphNode> resolvedStates = new ArrayList<>();
		    			Variable asymmetricObservedVariable = asymmetricObservedNodes.get(0).getVariable();
		    			List<Variable> evidenceVariables = new ArrayList<>();
		    			evidenceVariables.add(asymmetricObservedVariable);
		    			for(Variable variable : alwaysObservedVariables)
		    			{
		    				if(!evidenceVariables.contains(variable))
		    					evidenceVariables.add(variable);
		    			}
						TablePotential newEvidenceProbability = new TablePotential(evidenceVariables, PotentialRole.JOINT_PROBABILITY);
						for(int i=0; i< asymmetricObservedVariable.getNumStates() ; ++i)
						{
							State state = asymmetricObservedVariable.getStates()[i];
							//System.out.println(asymmetricObservedVariable.getName() + "=" + state.getName());
							root = new DecompositionGraphNode(asymmetricObservedVariable, i);
							root.setParent(parentNode);
		    				ProbNet instantiatedNet = probNet.copy();
	    					// Project potentials depending on the value assigned to the always-observed variable in this scenario
	    					projectPotentials(instantiatedNet, asymmetricObservedVariable, state);
	    					// 	Reveal, apply restrictions and prune model
	    					List<Node> revealedNodes = instantiate(instantiatedNet, asymmetricObservedVariable, state);
	    					DecompositionGraphNode node = resolve(instantiatedNet, root);
							TablePotential expectedUtility = node.getExpectedUtility();
							TablePotential evidenceProbability = node.getScenarioProbability();
							for(Node revealedNode : revealedNodes)
							{
								Variable revealedVariable = revealedNode.getVariable();
								List<TablePotential> potentials = new ArrayList<>();
								//TablePotential marginalProbability = getMarginalProbability(revealedVariable, originalProbNet, root, alwaysObservedVariables);
								List<Variable> potentialVariables = evidenceProbability.getVariables();
								int index = potentialVariables.indexOf(revealedVariable);
								if(index != -1)
								{
									potentialVariables.set(index, potentialVariables.get(0));
									potentialVariables.set(0, revealedVariable);
									TablePotential conditionalProbability = DiscretePotentialOperations.reorder(evidenceProbability, potentialVariables);
									DiscretePotentialOperations.normalize(conditionalProbability);
									potentials.add(conditionalProbability);
									potentials.add(expectedUtility);
		    						expectedUtility = DiscretePotentialOperations.sumOutVariable(revealedVariable, potentials).get(0);
		    						evidenceProbability = DiscretePotentialOperations.sumOutVariable(revealedVariable, Arrays.asList(evidenceProbability)).get(0);
								}
								
								// Eliminate unobserved variables from utility
								Set<Variable> variablesToEliminate = new HashSet<>(expectedUtility.getVariables());
								variablesToEliminate.removeAll(alwaysObservedVariables);
								while(!variablesToEliminate.isEmpty())
								{
									Variable variableToEliminate = variablesToEliminate.iterator().next();
									Potential potential = probNet.getNode(variableToEliminate).getPotentials().get(0);
									variablesToEliminate.addAll(potential.getVariables());
									variablesToEliminate.remove(variableToEliminate);
									List<TablePotential> projectedPotentials = potential.tableProject(new EvidenceCase(), null);
//									TablePotential probability = DiscretePotentialOperations.multiplyAndMarginalize(projectedPotentials, variableToEliminate);
//									projectedPotentials.add(expectedUtility);
//									expectedUtility = DiscretePotentialOperations.multiplyAndMarginalize(projectedPotentials, variableToEliminate);
									List<TablePotential> marginalizedPotentialsProbability = DiscretePotentialOperations.sumOutVariable(variableToEliminate, projectedPotentials);
									expectedUtility = marginalizedPotentialsProbability.get(0);
								}
							}
							for(int j = 0; j<evidenceProbability.values.length;++j)
							{
								if(newEvidenceProbability.values.length <=i+j*asymmetricObservedVariable.getNumStates())
									System.out.println("WTH");
								newEvidenceProbability.values[i+j*asymmetricObservedVariable.getNumStates()]=evidenceProbability.values[j];
							}
							List<Variable> variables = expectedUtility.getVariables();
							Collections.sort(variables);
							expectedUtility = DiscretePotentialOperations.reorder(expectedUtility, variables);
							node.setExpectedUtility(expectedUtility);
							resolvedStates.add(node);
						}
						root.setScenarioProbability(newEvidenceProbability);
						// Join all states into single utility potential 
						List<Variable> potentialVariables = resolvedStates.get(0).getExpectedUtility().getVariables();
						potentialVariables.add(asymmetricObservedVariable);
						TablePotential expectedUtility = new TablePotential(potentialVariables, PotentialRole.UTILITY);
						expectedUtility.interventions = new Intervention[expectedUtility.getTableSize()];
						for(int i=0; i<resolvedStates.size(); ++i)
						{
							TablePotential newExpectedUtility = resolvedStates.get(i).getExpectedUtility();
							for(int j=0; j<newExpectedUtility.getTableSize();++j)
							{
								expectedUtility.values[i*newExpectedUtility.getTableSize()+j] = newExpectedUtility.values[j];
								expectedUtility.interventions[i*newExpectedUtility.getTableSize()+j] = newExpectedUtility.interventions[j];
							}
						}
						List<Variable> variables = expectedUtility.getVariables();
						Collections.sort(variables);
						expectedUtility = DiscretePotentialOperations.reorder(expectedUtility, variables);
						root.setExpectedUtility(expectedUtility);
		    		}else
		    		{
		    			List<Node> nextDecisions = getNextDecisions(probNet);
		    			List<TablePotential> resolvedDecisions = new ArrayList<>();
		    			for(Node nextDecision : nextDecisions)
		    			{
		        			List<DecompositionGraphNode> resolvedStates = new ArrayList<>();
		        			Variable decisionVariable = nextDecision.getVariable();
		        			State[] decisionStates = decisionVariable.getStates();
	    					for(int i=0; i< decisionStates.length ; ++i)
							{
								State state = decisionStates[i];
								//System.out.println(decisionVariable.getName() + "=" + state.getName());
			    				root = new DecompositionGraphNode(decisionVariable, i);
			    				root.setParent(parentNode);
			    				ProbNet instantiatedNet = probNet.copy();
		    					Node decisionNode =  instantiatedNet.getNode(decisionVariable);
		    					// Project potentials depending on decision to this particular state
		    					projectPotentials(instantiatedNet, decisionVariable, state);
		    					// 	Reveal, apply restrictions and prune model
		    					List<Node> revealedNodes = instantiate(instantiatedNet, decisionVariable, state);
		    					instantiatedNet.removeNode(decisionNode);
		    					DecompositionGraphNode node = resolve(instantiatedNet, root);
		    					TablePotential expectedUtility = node.getExpectedUtility();
								TablePotential evidenceProbability = node.getScenarioProbability();
		    					for(Node revealedNode : revealedNodes)
		    					{
		    						Variable revealedVariable = revealedNode.getVariable();
									List<TablePotential> potentials = new ArrayList<>();
									//TablePotential marginalProbability = getMarginalProbability(revealedVariable, originalProbNet, root, alwaysObservedVariables);
									List<Variable> potentialVariables = evidenceProbability.getVariables();
									int index = potentialVariables.indexOf(revealedVariable);
									if(index!=-1)
									{
										potentialVariables.set(index, potentialVariables.get(0));
										potentialVariables.set(0, revealedVariable);
										TablePotential conditionalProbability = DiscretePotentialOperations.reorder(evidenceProbability, potentialVariables);
										DiscretePotentialOperations.normalize(conditionalProbability);
										potentials.add(conditionalProbability);
										potentials.add(expectedUtility);
			    						expectedUtility = DiscretePotentialOperations.sumOutVariable(revealedVariable, potentials).get(0);
			    						List<TablePotential> marginalizedPotentials = DiscretePotentialOperations.sumOutVariable(revealedVariable, Arrays.asList(evidenceProbability)); 
			    						evidenceProbability = (!marginalizedPotentials.isEmpty())? marginalizedPotentials.get(0) : new TablePotential(new ArrayList<Variable>(), PotentialRole.JOINT_PROBABILITY);
									}
		    						//expectedUtility = DiscretePotentialOperations.divide(expectedUtility, marginalProbability);
		    					}
								root.setScenarioProbability(evidenceProbability);
		    					// Make sure always observed variables are in all potentials
	    						if(!expectedUtility.getVariables().containsAll(alwaysObservedVariables))
	    						{
	    							List<Variable> potentialVariables = expectedUtility.getVariables();
	    							for(Variable alwaysObservedVariable : alwaysObservedVariables)
	    								if(!potentialVariables.contains(alwaysObservedVariable))
	    									potentialVariables.add(alwaysObservedVariable);
	    							
	    							double[] originalValues = expectedUtility.values; 
	    							Intervention[] originalInterventions = expectedUtility.interventions;
	    							expectedUtility = new TablePotential(potentialVariables, expectedUtility.getPotentialRole());
	    							if(originalInterventions != null)
	    								expectedUtility.interventions = new Intervention[expectedUtility.getTableSize()];
	    							int ind=0;
	    							while(ind<expectedUtility.getTableSize())
	    							{
	    								for(int j=0; j<originalValues.length;++j)
	    								{
	    									expectedUtility.values[ind++] = originalValues[j];
	    									if(originalInterventions != null)
	    										expectedUtility.interventions[ind++] = originalInterventions[j];
	    								}
	    							}
	    						}
								List<Variable> variables = expectedUtility.getVariables();
								Collections.sort(variables);
								expectedUtility = DiscretePotentialOperations.reorder(expectedUtility, variables);
		    					node.setExpectedUtility(expectedUtility);
		    					resolvedStates.add(node);    				
		    				}
	    					// Join by maximization
		    				TablePotential expectedUtility = resolvedStates.get(0).getExpectedUtility();
		    				List<List<State>> optimalStates = new ArrayList<>();
		    				List<List<Intervention>> optimalInterventions = new ArrayList<>();
		    				for(int j=0; j<expectedUtility.values.length;++j)
	    					{
		    					optimalStates.add(new ArrayList<State>());
		    					optimalStates.get(j).add(decisionStates[0]);
		    					optimalInterventions.add(new ArrayList<Intervention>());
		    					optimalInterventions.get(j).add(expectedUtility.interventions != null? expectedUtility.interventions[j] : null);
	    					}
		    				for(int i=1; i<resolvedStates.size(); ++i)
		    				{
		    					State decisionState = decisionStates[i];
		    					TablePotential newExpectedUtility = resolvedStates.get(i).getExpectedUtility();
		    					for(int j=0; j<expectedUtility.values.length;++j)
		    					{
		    						if(newExpectedUtility.values[j] >= expectedUtility.values[j])
		    						{
		    							if(newExpectedUtility.values[j] > expectedUtility.values[j])
		    							{
		    								optimalStates.set(j, new ArrayList<State>());
		    								optimalInterventions.set(j, new ArrayList<Intervention>());
		    							}
		    							expectedUtility.values[j] =  newExpectedUtility.values[j];
		    							optimalStates.get(j).add(decisionState);
		    							optimalInterventions.get(j).add(newExpectedUtility.interventions != null ? newExpectedUtility.interventions[j] : null);
		    						}
		    					}
		    				}
		    				if(expectedUtility.interventions == null)
		    				{
		    					expectedUtility.interventions = new Intervention[expectedUtility.getTableSize()];
		    				}
		    				for(int j=0; j<expectedUtility.values.length;++j)
	    					{
		    					expectedUtility.interventions[j] = (optimalInterventions.get(j).get(0) != null)? new Intervention(decisionVariable, optimalStates.get(j), optimalInterventions.get(j)) : new Intervention(decisionVariable, optimalStates.get(j));
	    					}
		    				resolvedDecisions.add(expectedUtility);
		    			}
		    			//  Join by maximization
						TablePotential expectedUtility = resolvedDecisions.get(0);
						for(int i=1; i<resolvedDecisions.size(); ++i)
						{
							TablePotential newExpectedUtility = resolvedDecisions.get(i);
							for(int j=0; j<expectedUtility.values.length;++j)
							{
								if(newExpectedUtility.values[j] > expectedUtility.values[j])
								{
									expectedUtility.values[j] =  newExpectedUtility.values[j];
									expectedUtility.interventions[j] = newExpectedUtility.interventions[j];
								}
							}
						}
						root.setExpectedUtility(expectedUtility);
		    		}
		    	}else // DAN is symmetric
		    	{
		    		root = resolveSymmetricDAN(probNet, parentNode, scenario,
							alwaysObservedVariables);
		    	}
				if(useCache)
					cache.put(scenario, new PartialResult(root.getExpectedUtility(), root.getScenarioProbability()));
			}else
			{
				root = new DecompositionGraphNode();
	    		root.setParent(parentNode);
	    		PartialResult partialResult = cache.get(scenario);
				root.setExpectedUtility(partialResult.utilityPotential);
    			root.setScenarioProbability(partialResult.probabilityPotential);
			}

    	}catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    	//System.out.println(root.toString());
        return root;
    }

	private DecompositionGraphNode resolveSymmetricDAN(ProbNet probNet,
			DecompositionGraphNode parentNode, Scenario scenario,
			List<Variable> alwaysObservedVariables)
			throws NotEvaluableNetworkException, IncompatibleEvidenceException,
			UnexpectedInferenceException, Exception {
		DecompositionGraphNode root = null;
//		root = new DecompositionGraphNode();
//		root.setParent(parentNode);
////		VariableElimination variableElimination = new VariableElimination(probNet);
////		variableElimination.setConditioningVariables(alwaysObservedVariables);
//		// TODO build the optimal strategy tree
//		//Potential optimalStrategy = buildOptimalStrategyTree(variableElimination, probNet);
////		TablePotential expectedUtility = variableElimination.getGlobalUtility();
//		List<Variable> variables = expectedUtility.getVariables();
//		Collections.sort(variables);
//		expectedUtility = DiscretePotentialOperations.reorder(expectedUtility, variables);
//		root.setExpectedUtility(expectedUtility);
//		root.setScenarioProbability(getScenarioProbability(probNet, alwaysObservedVariables, scenario));
		return root;
	}
    
    private TablePotential getScenarioProbability(ProbNet probNet, List<Variable> conditioningVariables, Scenario scenario) throws Exception
    {
		EvidenceCase evidence = new EvidenceCase();
		for(int i=0; i<scenario.variables.size();++i)
		{
			evidence.addFinding(new Finding(scenario.variables.get(i), scenario.states.get(i)));
		}
		List<TablePotential> projectedPotentials = new ArrayList<>();
		for(Potential potential : probNet.getPotentials())
		{
			if(!potential.isUtility())
				projectedPotentials.addAll(potential.tableProject(evidence, null));
		}
		TablePotential scenarioProbability = DiscretePotentialOperations.multiplyAndMarginalize(projectedPotentials, conditioningVariables);
		return scenarioProbability;

    }

	private HashMap<Variable,Integer> getScenarioMap(DecompositionGraphNode parentNode)
			throws InvalidStateException, IncompatibleEvidenceException {
		HashMap<Variable,Integer> scenarioMap = new LinkedHashMap<>();
		DecompositionGraphNode node = parentNode;
		while(node != null)
		{
			scenarioMap.put(node.getAsymmetricVariable(), node.getState());
			node = node.getParent();
		}
		return scenarioMap;
	}

	/**
	 * Project potentials where this variable appears.
	 * @param probNet
	 * @param variable
	 * @param state
	 * @throws InvalidStateException
	 * @throws IncompatibleEvidenceException
	 * @throws NonProjectablePotentialException
	 * @throws WrongCriterionException
	 */
	private static void projectPotentials(ProbNet probNet, Variable variable, State state)
			throws InvalidStateException, IncompatibleEvidenceException,
			NonProjectablePotentialException, WrongCriterionException {
		
    	EvidenceCase decisionEvidence = new EvidenceCase();
    	decisionEvidence.addFinding(new Finding(variable, state));
		for(Potential potential : probNet.getPotentials(variable))
		{
			List<TablePotential> projectedPotentials = potential.tableProject(decisionEvidence, null);
			probNet.getNode(potential.getConditionedVariable()).setPotential(projectedPotentials.get(0));
		}    	
    }

	private static List<Node> getNextDecisions(ProbNet probNet) {
		
		List<Node> decisionNodes = ProbNetOperations.getParentlessDecisions(probNet);
		// Check if the nodes revealed by a decision node are the subset of another
		// In that case we don't need to consider them as valid orders
		List<List<Node>> revealedNodes = new ArrayList<>();
		for (Node node : decisionNodes) {
			List<Node> revealedByDecision = new ArrayList<>();
			for (Link<Node> link : node.getLinks()) {
				if (link.getNode1().equals(node) && link.hasRevealingConditions()) {
					revealedByDecision.add((Node) link.getNode2());
				}
			}
			revealedNodes.add(revealedByDecision);
		}
		List<Node> dominatedDecisions = new ArrayList<>();
		for (int i=0; i<decisionNodes.size(); ++i) {
			Node nodeA = decisionNodes.get(i);
			if(!revealedNodes.get(i).isEmpty())
			{
				for (int j=0; j<decisionNodes.size(); ++j) {
					Node nodeB = decisionNodes.get(j);
					if (nodeA != nodeB
							&& revealedNodes.get(i).containsAll(revealedNodes.get(j)))
						dominatedDecisions.add(nodeB);
				}
			}
		}
		decisionNodes.removeAll(dominatedDecisions);

		return decisionNodes;
	}

	private static List<Node> getAsymmetricObservableNodes(List<Node> alwaysObservedNodes) {
		
		List<Node> asymmetricObservableNodes = new ArrayList<>();
		
		for(Node alwaysObservedNode : alwaysObservedNodes)
		{
			boolean hasAsymmetry = false;
			for (Link<Node> link : alwaysObservedNode.getLinks()) {
				hasAsymmetry |= link.getNode1().equals(alwaysObservedNode) && link.hasRestrictions();
			}
			if(hasAsymmetry)
				asymmetricObservableNodes.add(alwaysObservedNode);
		}
		
		return asymmetricObservableNodes;
	}

	/**
	 * Applies restrictions, reveals and removes totally restricted nodes (and descendants)
	 * @param probNet
	 * @param variable
	 * @param state
	 * @return
	 * @throws Exception
	 */
	private List<Node> instantiate(ProbNet probNet, Variable variable, State state)
			throws Exception    {

		Node node = probNet.getNode(variable);
    	List<Node> revealedVariables = new ArrayList<>();
		for (Link<Node> link : node.getLinks ())
        {
            if(link.getNode1 ().equals (node)) // Our node is the source node
            {
            	// Remove link 
            	probNet.removeLink(link);

            	// Reveal nodes
                Node destinationNode = link.getNode2 ();
                if(destinationNode.getNodeType () == NodeType.CHANCE)
                {
                    if (link.hasRevealingConditions ())
                    {
                        if (link.getRevealingStates ().contains (state))
                        {
                        	List<Node> predecessorDecisions = ProbNetOperations.getPredecessorDecisions(destinationNode, probNet);
                        	// If it has predecessor decisions, do not reveal it yet, but add revealing links
                        	// from every predecessor decision to the node
                        	if(predecessorDecisions.isEmpty())
                        	{
                        		destinationNode.setAlwaysObserved (true);
                                revealedVariables.add(destinationNode);

                        	}else
                        	{
                        		for(Node predecessorDecision : predecessorDecisions)
                        		{
                        			Link<Node> revealingArc = probNet.addLink(predecessorDecision, destinationNode, true);
                        			State[] predecessorDecisionStates = predecessorDecision.getVariable().getStates();
                        			for(int i=0; i<predecessorDecisionStates.length;++i)
                        				revealingArc.addRevealingState(predecessorDecisionStates[i]);
                        		}
                        	}
                            destinationNode.setAlwaysObserved (true);
                        }
                    }
                }
            	State[] restrictedVariableStates = destinationNode.getVariable ().getStates ();

                if(link.hasRestrictions ())
                {
                	//Apply restrictions
                    List<State> nonRestrictedStates = ProbNetOperations.getUnrestrictedStates(link, restrictedVariableStates, state);
                
                    if(nonRestrictedStates.isEmpty ())
                    {
                        // Remove destination node and its descendants!
                        Stack<Node> disposableNodes = new Stack<> ();
                        disposableNodes.push (destinationNode);
                        while(!disposableNodes.isEmpty ())
                        {
                            Node disposableNode = disposableNodes.pop ();
                            // If it's a decision node, check if there is another 
                            // path to it from another decision
                            if(disposableNode.getNodeType() != NodeType.DECISION ||
                                !ProbNetOperations.hasPredecessorDecision(disposableNode, probNet))
                            {
                                for(Node descendant : disposableNode.getChildren ())
                                {
                                    disposableNodes.push(descendant);
                                }
                                probNet.removeNode (disposableNode);
                            }
                        }
                        
                    }else if(nonRestrictedStates.size () == 1 && destinationNode.getNodeType() != NodeType.DECISION) 
                    {
                    	// Remove variables with a single state
                    	State remainingState = nonRestrictedStates.get (0);
                    	projectPotentials(probNet, destinationNode.getVariable(), remainingState);
                    	// Apply restrictions and reveal
                        List<Node> additionalRevealedVariables = instantiate(probNet, destinationNode.getVariable(), remainingState);
                        revealedVariables.addAll(additionalRevealedVariables);
                        probNet.removeNode (destinationNode);
                    }else if(nonRestrictedStates.size () < restrictedVariableStates.length)
                    {
                        // At least one of the states of the destination node is restricted.
                        // Make a copy of the variable and remove the restricted states
                    	Variable originalVariable = destinationNode.getVariable ();
                    	Variable restrictedVariable = getVariableFromPool(originalVariable, nonRestrictedStates);
                    	if( restrictedVariable == null)
                    	{
                            State[] unrestrictedStates = nonRestrictedStates.toArray (new State[0]);
                            restrictedVariable = new Variable (originalVariable.getName (), unrestrictedStates);
                            restrictedVariablePool.add(restrictedVariable);
                    	}
                        // Adapt potentials
                        adaptPotentials(originalVariable, restrictedVariable, probNet);
                        restrictedVariable.setVariableType (destinationNode.getVariable ().getVariableType ());
                        destinationNode.setVariable(restrictedVariable);
                    }
                }
            }
         }
		// Make sure we have removed all outgoing links from this node
		for (Link<Node> link : node.getLinks ())
        {
			if(link.getNode1 ().equals (node)) // Our node is the source node
            {
				probNet.removeLink(link);
            }
        }
		return revealedVariables;
    }

	private Variable getVariableFromPool(Variable originalVariable, List<State> nonRestrictedStates) {
		int i=0;
		Variable restrictedVariable = null;
		while(i<restrictedVariablePool.size() && restrictedVariable == null)
		{
			Variable poolVariable = restrictedVariablePool.get(i);
			if(poolVariable.getName().equals(originalVariable.getName()) &&
					poolVariable.getNumStates() == nonRestrictedStates.size())
			{
				boolean sameStates = true;
				State[] poolVariableStates = poolVariable.getStates();
				for(int j=0;j<poolVariable.getNumStates();++j)
					sameStates &= poolVariableStates[j].equals(nonRestrictedStates.get(j));
				if(sameStates)
					restrictedVariable = poolVariable;
			}
				
			++i;
		}
		return restrictedVariable;
	}

	private static void adaptPotentials(Variable originalVariable, Variable restrictedVariable, ProbNet probNet) throws Exception {
		List<Potential> potentials = probNet.getPotentials(originalVariable);
		boolean[] isRestricted = new boolean[originalVariable.getStates().length];
		State[] originalStates = originalVariable.getStates();
		State[] restrictedStates = restrictedVariable.getStates();
		for(int i=0; i<originalStates.length; ++i)
		{
			isRestricted[i] = true;
			for(int j=0; j<restrictedStates.length; ++j)
				isRestricted[i] &= originalStates[i] != restrictedStates[j];
		}
		
		for(Potential potential : potentials)
		{
			Potential newPotential = null;
			if(potential instanceof TablePotential)
			{
				TablePotential originalTablePotential = (TablePotential) potential;
				List<Variable> variables = originalTablePotential.getVariables();
				int[] offsets = originalTablePotential.getOffsets();
				int index = variables.indexOf(originalVariable);
				variables.set(index, restrictedVariable);
				TablePotential restrictedTablePotential = new TablePotential(variables, originalTablePotential.getPotentialRole()); 
				int i = 0;
				for(int j=0; j <originalTablePotential.values.length; ++j)
				{
					if(!isRestricted[(j / offsets[index]) % originalStates.length])
						restrictedTablePotential.values[i++] = originalTablePotential.values[j]; 
				}
				newPotential = restrictedTablePotential;
			}else {
				// TODO implement it for other types of potentials
				throw new Exception("Filtering of potentials of unrestricted states is not implemented for this type of potential");
			}
			probNet.getNode(potential.getConditionedVariable()).setPotential(newPotential);
		}
		
	}

	@Override
	public Intervention getOptimalStrategy() throws IncompatibleEvidenceException,
			UnexpectedInferenceException {
		if(!isResolved)
			resolve();		
		return globalUtility.interventions[0];
	}    	
	
}

/*
 * Copyright 2014 CISIAD, UNED, Spain
 *
 * Licensed under the European Union Public Licence, version 1.1 (EUPL)
 *
 * Unless required by applicable law, this code is distributed
 * on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
 */

package org.openmarkov.inference.decompositionIntoSymmetricDANs;

import java.util.List;

import org.openmarkov.core.model.network.Variable;
import org.openmarkov.core.model.network.potential.TablePotential;

public class DecompositionGraphNode {
	private List<Variable> revealedVariables;
	private Variable asymmetricVariable;
	private int state;
	private DecompositionGraphNode parent = null;
	private TablePotential expectedUtility;
	private TablePotential scenarioProbability;

	public DecompositionGraphNode()
	{
		this.asymmetricVariable = null;
		this.state = -1;
	}
	public DecompositionGraphNode(Variable asymmetricVariable, int state)
	{
		this.asymmetricVariable = asymmetricVariable;
		this.state = state;
	}
	public List<Variable> getRevealedVariables() {
		return revealedVariables;
	}
	public void setRevealedVariables(List<Variable> variables) {
		this.revealedVariables = variables;
	}
	public Variable getAsymmetricVariable() {
		return asymmetricVariable;
	}
	public void setAsymmetricVariable(Variable variable) {
		this.asymmetricVariable = variable;
	}
	public DecompositionGraphNode getParent() {
		return parent;
	}
	public void setParent(DecompositionGraphNode parent) {
		this.parent = parent;
	}
	public void setExpectedUtility(TablePotential globalUtility) {
		this.expectedUtility = globalUtility;
	}
	public TablePotential getExpectedUtility() {
		return expectedUtility;
	}
	
	public String toString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append(asymmetricVariable != null?asymmetricVariable.toString() + "=" + asymmetricVariable.getStateName(state) + ";": "");
		sb.append(expectedUtility != null?expectedUtility.toString() + ";" : "");
		sb.append(revealedVariables != null? revealedVariables.toString() + ";" : "");
		sb.append(scenarioProbability != null? scenarioProbability.toString() : "");
		return sb.toString();
	}
	public int getState() {
		return state;
	}
	public void setScenarioProbability(TablePotential evidenceProbability) {
		this.scenarioProbability = evidenceProbability;
	}
	public TablePotential getScenarioProbability() {
		return this.scenarioProbability;
	}
}

package org.openmarkov.inference.decompositionIntoSymmetricDANs;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.openmarkov.core.exception.IncompatibleEvidenceException;
import org.openmarkov.core.exception.NodeNotFoundException;
import org.openmarkov.core.exception.NotEvaluableNetworkException;
import org.openmarkov.core.exception.UnexpectedInferenceException;
import org.openmarkov.core.inference.BasicOperations;
import org.openmarkov.core.model.network.EvidenceCase;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.core.model.network.factory.DANFactory;
import org.openmarkov.core.model.network.potential.TablePotential;

public class DecompositionAlgorithmTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testDecideTestDAN() throws IncompatibleEvidenceException, UnexpectedInferenceException, NodeNotFoundException, NotEvaluableNetworkException{
		ProbNet decideTestDAN = DANFactory.buildDecideTestDAN();
		long startTime = System.nanoTime();
		DecompositionAlgorithm recursiveEvaluation = new DecompositionAlgorithm(decideTestDAN);
		TablePotential globalUtility = recursiveEvaluation.getGlobalUtility();
		long ellapsedTime = (System.nanoTime() - startTime) / 1000000;
		System.out.println(" Execution time =" +ellapsedTime);
		Assert.assertEquals(9.3929, globalUtility.values[0], 0.0001);
		Assert.assertNotNull(recursiveEvaluation.getOptimalStrategy());
	}

	@Test
	public void testDiabetesDAN() throws NodeNotFoundException, IncompatibleEvidenceException,
			UnexpectedInferenceException, NotEvaluableNetworkException {

		ProbNet diabetesDAN = DANFactory.buildDiabetesDAN();
		long startTime = System.nanoTime();
		DecompositionAlgorithm recursiveEvaluation = new DecompositionAlgorithm(diabetesDAN);
		TablePotential globalUtility = recursiveEvaluation.getGlobalUtility();
		long ellapsedTime = (System.nanoTime() - startTime) / 1000000;
		System.out.println(" Execution time =" +ellapsedTime);
		Assert.assertEquals(9.8261, globalUtility.values[0], 0.0001);
		Assert.assertNotNull(recursiveEvaluation.getOptimalStrategy());
	}

	@Test
	public void testDatingDAN() throws NodeNotFoundException, NotEvaluableNetworkException, IncompatibleEvidenceException, UnexpectedInferenceException {
		ProbNet datingDAN = DANFactory.buildDatingDAN();
		long startTime = System.nanoTime();
		DecompositionAlgorithm recursiveEvaluation = new DecompositionAlgorithm(datingDAN);
		TablePotential globalUtility = recursiveEvaluation.getGlobalUtility();
		long ellapsedTime = (System.nanoTime() - startTime) / 1000000;
		System.out.println(" Execution time =" +ellapsedTime);
		Assert.assertEquals(9.4076, globalUtility.values[0], 0.0001);
		Assert.assertNotNull(recursiveEvaluation.getOptimalStrategy());
	}

	@Test
	public void testReactorDAN() throws NodeNotFoundException, IncompatibleEvidenceException,
			UnexpectedInferenceException, NotEvaluableNetworkException {

		ProbNet reactorDAN = DANFactory.buildReactorDAN();
		long startTime = System.nanoTime();
		DecompositionAlgorithm recursiveEvaluation = new DecompositionAlgorithm(reactorDAN);
		TablePotential globalUtility = recursiveEvaluation.getGlobalUtility();
		long ellapsedTime = (System.nanoTime() - startTime) / 1000000;
		System.out.println(" Execution time =" +ellapsedTime);
		Assert.assertEquals(10.0627, globalUtility.values[0], 0.0001);
	}

	@Test
	public void testWooerDAN() throws NodeNotFoundException, IncompatibleEvidenceException,
			UnexpectedInferenceException, NotEvaluableNetworkException {

		ProbNet wooerDAN = DANFactory.buildWooerDAN();
		long startTime = System.nanoTime();
		DecompositionAlgorithm recursiveEvaluation = new DecompositionAlgorithm(wooerDAN);
		TablePotential globalUtility = recursiveEvaluation.getGlobalUtility();
		long ellapsedTime = (System.nanoTime() - startTime) / 1000000;
		System.out.println(" Execution time =" +ellapsedTime);
		Assert.assertEquals(7.73, globalUtility.values[0], 0.0001);
	}

	@Test
	public void testUsedCarBuyerDAN() throws NodeNotFoundException, IncompatibleEvidenceException,
			UnexpectedInferenceException, NotEvaluableNetworkException {

		ProbNet usedCarBuyerDAN = DANFactory.buildUsedCarBuyer();
		long startTime = System.nanoTime();
		DecompositionAlgorithm recursiveEvaluation = new DecompositionAlgorithm(usedCarBuyerDAN);
		TablePotential globalUtility = recursiveEvaluation.getGlobalUtility();
		long ellapsedTime = (System.nanoTime() - startTime) / 1000000;
		System.out.println(" Execution time =" +ellapsedTime);
		Assert.assertEquals(32.96, globalUtility.values[0], 0.0001);
	}

	@Test
	public void testMediastiNetDAN() throws NodeNotFoundException, IncompatibleEvidenceException,
			UnexpectedInferenceException, NotEvaluableNetworkException {

		ProbNet mediastiNetDAN = DANFactory.buildMediastinetDAN();
		long startTime = System.nanoTime();
		mediastiNetDAN = BasicOperations.removeSuperValueNodes(mediastiNetDAN, new EvidenceCase());
		DecompositionAlgorithm recursiveEvaluation = new DecompositionAlgorithm(mediastiNetDAN);
		TablePotential globalUtility = recursiveEvaluation.getGlobalUtility();
		long ellapsedTime = (System.nanoTime() - startTime) / 1000000;
		System.out.println(" Execution time =" +ellapsedTime);
		Assert.assertEquals(1.4710368294106826, globalUtility.values[0], 0.000001);
	}

	@Test
	public void testNtests() throws NodeNotFoundException, IncompatibleEvidenceException,
			UnexpectedInferenceException, NotEvaluableNetworkException {

		ProbNet nTestsDAN = DANFactory.buildNTestsDAN(4);
		long startTime = System.nanoTime();
		DecompositionAlgorithm recursiveEvaluation = new DecompositionAlgorithm(nTestsDAN);
		TablePotential globalUtility = recursiveEvaluation.getGlobalUtility();
		long ellapsedTime = (System.nanoTime() - startTime) / 1000000;
		System.out.println(" Execution time =" +ellapsedTime);
	}

	@Test
	public void testTwoPhasesOfTests() throws NodeNotFoundException, IncompatibleEvidenceException,
	UnexpectedInferenceException, NotEvaluableNetworkException {
		ProbNet twoPhasesOfTestsDAN = DANFactory.buildTwoPhasesOfTestsDAN(2);
		DecompositionAlgorithm recursiveEvaluation = new DecompositionAlgorithm(twoPhasesOfTestsDAN);
		TablePotential globalUtility = recursiveEvaluation.getGlobalUtility();

	}
}
